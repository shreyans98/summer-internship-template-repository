import React from 'react';
import { Timeline, Icon } from 'antd';

const Timeline1 = (props) => {
    return (
        <Timeline mode="alternate">
            <Timeline.Item>Michael Dell founded PC's Limited in 1984 </Timeline.Item>
            <Timeline.Item color="green">In 1987 the company launched its first computer system </Timeline.Item>
            <Timeline.Item dot={<Icon type="clock-circle-o" style={{ fontSize: '16px' }} />}>
            In 1988 the company was officially named DELL Technologies
        </Timeline.Item>
            <Timeline.Item color="red"></Timeline.Item>
            <Timeline.Item>In 2015 DELL Customer satisfaction rates reach record highs as customers feel the effects of Dell's singular focus as a private company.</Timeline.Item>
            <Timeline.Item dot={<Icon type="clock-circle-o" style={{ fontSize: '16px' }} />}>
                In 2016 DELL acquired EMC making it the biggest tech deal in the history.
        </Timeline.Item>
        </Timeline>
    )
}

export default Timeline1
